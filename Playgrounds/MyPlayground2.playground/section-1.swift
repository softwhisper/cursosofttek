// Playground - noun: a place where people can play

import UIKit
import Foundation

var str = "Hello, playground"
str = "Pablo"

class Curso: NSObject {
    let nombre = "Curso iOS"
    var asistentes:Int = 0
    
    init(asistentes: Int) {
        self.asistentes = asistentes
    }
    
    func infoCurso(#usuario:String,_ admin:String) -> String {
        return "\(nombre) con \(asistentes) personas"
    }
}

var elCurso: Curso = Curso.init(asistentes: 15)
elCurso.asistentes = 12
elCurso.infoCurso(usuario:"l", "l")

var A = 2
var B = 4
swap(&A, &B)
"A: \(A) - B: \(B)"


let names = ["Pablo", "Nano", "David", "Ivan", "Vero"]
var reversed = sorted(names, {(s1: String, s2: String) -> Bool in return  s1 > s2 })
println(reversed)

var short_reverse = sorted(names, {s1, s2 in return s1 > s2})
println(short_reverse)
// Closure extremos
var xt_reverse = sorted(names, {return $0 > $1})
println(xt_reverse)
// How the hell

var hell_reverse = sorted(names, >)
var name:NSString = "lalllallala"
name.isEqualToString("murcia")

 