//
//  SWImageViewController.h
//  GestionDeFicheros
//
//  Created by Pablo Formoso Estada on 29/10/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWImageViewController : UIViewController

- (void)setData:(id)data;

@end
