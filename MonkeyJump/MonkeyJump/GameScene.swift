//
//  GameScene.swift
//  MonkeyJump
//
//  Created by Pablo Formoso Estada on 15/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
import SpriteKit

class GameScene: SKScene {
    
    var monkey:SKSpriteNode = SKSpriteNode.init()
    var repeatSeq:SKAction = SKAction.init()
    var moveRepeat:SKAction = SKAction.init()
    var monkeySpeed:NSTimeInterval = 1.0

    var score:Int = 0
    var scoreLabel = SKLabelNode.init()
    
    override func didMoveToView(view: SKView) {
        let bg = SKSpriteNode.init(imageNamed:"bg_1_1024x2048.png")
        bg.size = CGSizeMake(self.frame.width, self.frame.height)
        bg.position = CGPointMake(self.size.width/2, self.size.height/2);
        self.addChild(bg)
        
        scoreLabel = SKLabelNode(text: "\(score)")
        scoreLabel.fontSize = 42
        scoreLabel.position = CGPointMake(self.size.width/2, self.size.height-40)
        self.addChild(scoreLabel)
        
        let button = SKButton(defaultButtonImage: "btnjump_normal.png",
                                         activeButtonImage: "btnjump_pressed.png",
                                              buttonAction: jump)
        
        button.position = CGPointMake(self.frame.width-25, self.frame.height/2)
        self.addChild(button)
        
        
        monkey = SKSpriteNode(imageNamed: "monkey_walk_left_1")
        monkey.position = CGPointMake(self.frame.width-80, 70)
        self.addChild(monkey)
        
        var pos1:SKTexture = SKTexture.init(imageNamed: "monkey_walk_left_1")
        var pos2:SKTexture = SKTexture.init(imageNamed: "monkey_walk_left_2")
        var monkeyMove:SKAction = SKAction.animateWithTextures([pos1, pos2], timePerFrame: 0.2)
        moveRepeat = SKAction.repeatActionForever(monkeyMove)
        monkey.runAction(moveRepeat)
        
        var moveLeft = SKAction.moveToX(80, duration: monkeySpeed)
        var flipX = SKAction.scaleXTo(-1.0, duration: 0.1)
        var moveRight = SKAction.moveToX(self.frame.width-80, duration: monkeySpeed)
        var flipRightX = SKAction.scaleXTo(1.0, duration: 0.1)
        var moveSequece = SKAction.sequence([moveLeft,flipX,moveRight,flipRightX])
        repeatSeq = SKAction.repeatActionForever(moveSequece);
        monkey.runAction(repeatSeq);
        
    }
    
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {

        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    
    func jump() {
        if (monkey.position.x > 120) && (monkey.position.x < 160) {
            monkey.removeAllActions()
            
            var goAway = SKAction.moveToY(500, duration: 1)
            monkey.runAction(goAway, completion: { () -> Void in
                self.monkey.position = CGPointMake(self.frame.width-80, 70)
                self.monkey.runAction(self.moveRepeat)
                self.monkey.runAction(self.repeatSeq)
                self.monkey.speed = 1.0;
            })
            
            score += 1
            scoreLabel.text = "\(score)"
            
            if (score == 3) {
                var transition:SKTransition = SKTransition.crossFadeWithDuration(1.0)
                var newScene:KudosScene = KudosScene(size: CGSizeMake(480, 320))
                self.scene?.view?.presentScene(newScene)
            }
        } else {
            score -= 1
            scoreLabel.text = "\(score)"
            monkey.speed += 0.5
            if (score < -20) {
                monkey.speed = 1.0
            }
        }
    }
}
