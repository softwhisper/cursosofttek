//
//  KudosScene.swift
//  MonkeyJump
//
//  Created by Pablo Formoso Estada on 16/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

import SpriteKit

class KudosScene: SKScene {
    var kudosLabel = SKLabelNode.init()
    
    override func didMoveToView(view: SKView) {
        kudosLabel = SKLabelNode(text: "Kudos my monkey")
        kudosLabel.position = CGPointMake(self.size.width/2, self.size.height/2)
        kudosLabel.fontSize = 62
        self.addChild(kudosLabel)
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        for touch: AnyObject in touches {
                var transition:SKTransition = SKTransition.crossFadeWithDuration(1.0)
                var newScene:GameScene = GameScene(size: CGSizeMake(480, 320))
                self.scene?.view?.presentScene(newScene)
        }
    }
}
