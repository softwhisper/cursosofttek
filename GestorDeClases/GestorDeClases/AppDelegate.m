//
//  AppDelegate.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 3/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#define GMS_API_KEY @"AIzaSyATawCcp0jhZ2dZxFITFfnw1vJplprhJ0A"

#import <GoogleMaps/GoogleMaps.h>
#import "AppDelegate.h"
#import "Student.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

/*
 NSMutableArray *students = [NSMutableArray.alloc init];
 
 for (int i = 0; i <= 1000; i++) {
 Student *student = Student.new;
 student.name = @"Pablo";
 student.city = @"Santiago";
 student.lastname = @"Formoso";
 student.email = @"pablo@pabloformoso.com";
 student.avatar_url = @"www.marca.com";
 student.student_id = 999;
 
 [students addObject:student];
 }
 
 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 NSData *tmpData = [NSKeyedArchiver archivedDataWithRootObject:students];
 [defaults setObject:tmpData forKey:@"students"];
 [defaults setObject:@"1234567" forKey:@"user_password"];
 */
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GMSServices provideAPIKey:GMS_API_KEY];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings
                                                settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    // Notificaciones locales
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    NSDate *date = [formater dateFromString:@"10/12/2014 19:48"];

    UILocalNotification *notification = [[UILocalNotification alloc] init];
    [notification setFireDate:date];
    [notification setAlertBody:@"Hola soy una alerta local"];
    [notification setUserInfo:@{@"USERID": @"1234"}];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    return YES;
}

#pragma mark notification delegate
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, notification);
#endif
    
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
