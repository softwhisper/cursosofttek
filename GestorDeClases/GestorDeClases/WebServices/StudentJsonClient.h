//
//  StudentJsonClient.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 19/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "SWNetworkDelegate.h"
#import <Foundation/Foundation.h>

@interface StudentJsonClient : NSObject

- (void)getStudentForController:(id<SWNetworkDelegate>)aController andStudentId:(int)anId;

@end
