//
//  ResourcesJsonClient.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResourcesJsonClient : NSObject

- (void)getResourcesForController:(id<SWNetworkDelegate>)aController;

@end
