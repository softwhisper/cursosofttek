//
//  CreateStudentClient.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 24/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <AFNetworking/AFNetworking.h>
#import "CreateStudentClient.h"

@interface CreateStudentClient ()

@end

@implementation CreateStudentClient

- (void)createStudent:(Student *)student forController:(id<SWNetworkDelegate>)aController {
   
    NSString *urlString = @"http://curso.softwhisper.es/stundents.json";
    
    NSDictionary *parameters = @{
                                 @"stundent[name]": student.name,
                                 @"stundent[lastname]": student.lastname,
                                 @"stundent[email]": student.email,
                                 @"stundent[city]": student.city,
                                 };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:urlString
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              NSLog([responseObject description]);
              [aController receiveData:nil];
              
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [aController dataFailure:@"Error creando"];
    }];
}

@end
