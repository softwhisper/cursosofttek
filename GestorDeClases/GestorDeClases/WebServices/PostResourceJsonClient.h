//
//  PostResourceJsonClient.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
@class Resource;
#import <Foundation/Foundation.h>

@interface PostResourceJsonClient : NSObject

- (void)postResource:(Resource *)resource forController:(id<SWNetworkDelegate>)controller;

@end
