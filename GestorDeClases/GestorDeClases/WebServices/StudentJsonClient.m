//
//  StudentJsonClient.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 19/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "Student.h"
#import <AFNetworking/AFNetworking.h>
#import "StudentJsonClient.h"

@interface StudentJsonClient ()

@property (assign) int studentId;
@property (nonatomic, strong) id<SWNetworkDelegate> controller;

@end


@implementation StudentJsonClient

- (void)getStudentForController:(id<SWNetworkDelegate>)aController andStudentId:(int)anId {
    
    _controller = aController;
    
    NSString *urlString = [NSString stringWithFormat:@"http://curso.softwhisper.es/stundents/%i.json", anId];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifndef NDEBUG
        NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, responseObject);
#endif
        Student *student = [[Student alloc] initWithDictionary:responseObject];
        [_controller receiveStudent:student];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifndef NDEBUG
        NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
        [_controller dataFailure:[error localizedDescription]];
    }];
    
    [[NSOperationQueue mainQueue] addOperation:op];
    
    /*
    NSOperationQueue *ourQueue = [[NSOperationQueue alloc] init];
    [ourQueue setName:@"StudentsQueue"];
    [ourQueue setMaxConcurrentOperationCount:10000];
    for (int i = 0; i <= 100; i++) {
        [ourQueue addOperation:[op copy]];
    }
    
    [ourQueue cancelAllOperations];
    */
}

@end
