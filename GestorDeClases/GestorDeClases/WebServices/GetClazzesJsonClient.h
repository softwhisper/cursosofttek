//
//  GetClazzesJsonClient.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface GetClazzesJsonClient : NSObject 

- (void)getClazzesForController:(id<SWNetworkDelegate>) controller;

@end
