//
//  GetClazzesJsonClient.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <AFNetworking/AFNetworking.h>
#import "Clazz.h"
#import "GetClazzesJsonClient.h"

@implementation GetClazzesJsonClient


- (void)getClazzesForController:(id<SWNetworkDelegate>) controller {
    NSMutableArray *clazzes = NSMutableArray.new;
    NSString *urlString = @"http://curso.softwhisper.es/aecomo_classes.json";
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifndef NDEBUG
        NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, responseObject);
#endif
        
        for (NSDictionary *dic in responseObject) {
            Clazz *tmpRes = [[Clazz alloc] initWithDictionary:dic];
            [clazzes addObject:tmpRes];
        }
        
        [controller receiveData:clazzes];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifndef NDEBUG
        NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
        [controller dataFailure:[error localizedDescription]];
    }];
    
    [[NSOperationQueue mainQueue] addOperation:op];
    
}

@end
