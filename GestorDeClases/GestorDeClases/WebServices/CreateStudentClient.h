//
//  CreateStudentClient.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 24/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "Student.h"
#import <Foundation/Foundation.h>

@interface CreateStudentClient : NSObject


- (void)createStudent:(Student *)student forController:(id<SWNetworkDelegate>)aController;

@end
