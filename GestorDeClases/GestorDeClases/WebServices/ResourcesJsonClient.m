//
//  ResourcesJsonClient.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <AFNetworking/AFNetworking.h>
#import "ResourcesJsonClient.h"
#import "Resource.h"

@interface ResourcesJsonClient ()
@property (nonatomic, strong) NSMutableArray *resources;
@end


@implementation ResourcesJsonClient

- (void)getResourcesForController:(id<SWNetworkDelegate>)aController {
    NSString *urlString = @"http://curso.softwhisper.es/resources.json";
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifndef NDEBUG
        NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, responseObject);
#endif
        _resources = NSMutableArray.new;
        
        for (NSDictionary *dic in responseObject) {
            Resource *tmpRes = [[Resource alloc] initWithDictionary:dic];
            [_resources addObject:tmpRes];
        }
        
        [aController receiveData:_resources];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
#ifndef NDEBUG
        NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
        [aController dataFailure:[error localizedDescription]];
    }];
    
    [[NSOperationQueue mainQueue] addOperation:op];
    
}

@end
