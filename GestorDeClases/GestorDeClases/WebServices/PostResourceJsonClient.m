//
//  PostResourceJsonClient.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <AFNetworking/AFNetworking.h>
#import "Resource.h"
#import "PostResourceJsonClient.h"

@implementation PostResourceJsonClient

- (void)postResource:(Resource *)resource forController:(id<SWNetworkDelegate>)controller {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSData *imageData = UIImageJPEGRepresentation(resource.photo, 1.0);
    
    NSDictionary *parameters = @{
                                 @"resource[name]": resource.name,
                                 @"resource[description]": resource.rDescription,
                                 @"resource[link]": resource.link
                                 };

    [manager POST:@"http://curso.softwhisper.es/resources.json"
       parameters:parameters
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    [formData appendPartWithFileData:imageData
                                name:@"resource[photo]"
                            fileName:@"imagen"
                            mimeType:@"image/jpeg"];

    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [controller receiveData:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [controller dataFailure:@"Error"];
    }];
}

@end
