//
//  ImageCollectionViewCell.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 25/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
