//
//  STKAnnotation.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 3/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import "STKAnnotation.h"

@implementation STKAnnotation

- (id)initWithTitle:(NSString *)aTitle
           subtitle:(NSString *)aSubtitle
     andCoordinates:(CLLocationCoordinate2D)aCoordinate {
    self = [super init];
    if (self) {
        _title = aTitle;
        _subtitle = aSubtitle;
        _coordinate = aCoordinate;
    }
    return self;
}

@end
