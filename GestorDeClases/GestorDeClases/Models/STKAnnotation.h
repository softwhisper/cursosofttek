//
//  STKAnnotation.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 3/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface STKAnnotation : NSObject<MKAnnotation>

- (id)initWithTitle:(NSString *)aTitle
           subtitle:(NSString *)aSubtitle
     andCoordinates:(CLLocationCoordinate2D)aCoordinate;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;

@end
