//
//  Clazz.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Clazz : NSObject

- (id)initWithDictionary:(NSDictionary *)dic;

- (NSString *)getName;
- (NSString *)getContent;
- (NSString *)getEndsDate;
- (NSString *)getStartDate;

@end
