//
//  Resource.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Resource : NSObject

@property (assign) int rId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *rDescription;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *image_url;
@property (nonatomic, strong) UIImage *photo;

- (instancetype)initWithDictionary:(NSDictionary *)dic;

@end
