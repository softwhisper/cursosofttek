//
//  Clazz.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import "Clazz.h"

@interface Clazz ()
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSDate *start_at;
@property (nonatomic, strong) NSDate *ends_at;
@end

@implementation Clazz


- (id)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        _name = [dic objectForKey:@"name"];
        _content = [dic objectForKey:@"description"];
        
        // NSDateFormatter se usa para hacer parsing de fechas
        // NSDate+Utils
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        
        _start_at = [formatter dateFromString:[dic objectForKey:@"starts_at"]];
        _ends_at = [formatter dateFromString:[dic objectForKey:@"updated_at"]];
        
    }
    return self;
}


- (NSString *)getName {
    return _name;
}

- (NSString *)getContent {
    return _content;
}

- (NSString *)getStartDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    return [formatter stringFromDate:_start_at];
}

- (NSString *)getEndsDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    return [formatter stringFromDate:_ends_at];
}

@end
