//
//  Resource.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import "Resource.h"

@implementation Resource

- (instancetype)initWithDictionary:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        _rId = (int)[[dic objectForKey:@"id"] integerValue];
        _name = [dic objectForKey:@"name"];
        _rDescription = [dic objectForKey:@"description"];
        _link = [dic objectForKey:@"link"];
        _image_url = [dic objectForKey:@"image_url"];
    }
    return self;
}

@end
