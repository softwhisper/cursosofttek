//
//  StudentDetailViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 12/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "SWNetworkDelegate.h"
#import <UIKit/UIKit.h>

@interface StudentDetailViewController : UIViewController <SWNetworkDelegate>

@property (nonatomic, strong) NSString *email;
@property (assign) int studentId;

@end
