//
//  ClazzDetailViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
@class Clazz;
#import <UIKit/UIKit.h>

@interface ClazzDetailViewController : UIViewController

@property (nonatomic, strong) Clazz *clazz;

@end
