//
//  CreateResourceViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <MobileCoreServices/MobileCoreServices.h>
#import <UIKit/UIKit.h>

@interface CreateResourceViewController : UIViewController <SWNetworkDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end
