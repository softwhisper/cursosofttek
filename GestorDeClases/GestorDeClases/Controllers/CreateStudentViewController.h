//
//  CreateStudentViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 24/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateStudentViewController : UIViewController <UITextFieldDelegate, SWNetworkDelegate>

@end
