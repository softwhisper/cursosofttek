//
//  WebViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 2/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>

- (void)setUrlString:(NSString *)anUrl;

@end
