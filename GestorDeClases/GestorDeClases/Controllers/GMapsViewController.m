//
//  GMapsViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 2/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_LOWER_THAN_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0)

#import <GoogleMaps/GoogleMaps.h>
#import "GMapsViewController.h"
#import "STKAnnotation.h"

@interface GMapsViewController ()
@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (weak, nonatomic) IBOutlet MKMapView *mkMapView;
@property (nonatomic, strong) GMSMapView *gMap;
@end

@implementation GMapsViewController {
    SWLocationManager *manager;
    NSMutableArray *followHim;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    followHim = [[NSMutableArray alloc] init];
    
    GMSCameraPosition *cp = [GMSCameraPosition cameraWithLatitude:43.200000f
                                                        longitude:-8.400000f
                                                             zoom:10.0f];
    
    _gMap = [GMSMapView mapWithFrame:_mapView.frame
                              camera:cp];
    _gMap.myLocationEnabled = YES;
    [_gMap setMapType:kGMSTypeSatellite];
    [_gMap setDelegate:self];
    
    GMSMarker *pin = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(43.333322f, -8.442342f)];
    [pin setTitle:@"Curso"];
    [pin setSnippet:@"Hola que tal"];
    [pin setMap:_gMap];
    
    
    GMSMutablePath *path = [[GMSMutablePath alloc] init];
    [path addCoordinate:CLLocationCoordinate2DMake(43.332319f, -8.440342f)];
    [path addCoordinate:CLLocationCoordinate2DMake(43.331320f, -8.441330f)];
    [path addCoordinate:CLLocationCoordinate2DMake(43.333351f, -8.443320f)];
   
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    [polyline setStrokeWidth:3.0f];
    [polyline setStrokeColor:[UIColor redColor]];
    [polyline setMap:_gMap];
    
    [_mapView insertSubview:_gMap atIndex:0];
    
    // Location
    manager = [[SWLocationManager alloc] init];
    [manager.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    [manager.locationManager setDistanceFilter:kCLHeadingFilterNone]; // Double con la distancia en metros
    
    [manager setDelegate:self];
    
    if (IS_OS_8_OR_LATER) {
        [manager.locationManager requestAlwaysAuthorization];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [manager.locationManager startUpdatingLocation];
    [self dropPins];
}

- (void)viewWillDisappear:(BOOL)animated {
    [manager.locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SWLocationDelegate
- (void)locationUpdate:(CLLocation *)location {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    [followHim addObject:location];
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.002f, 0.002f);
    MKCoordinateRegion region = MKCoordinateRegionMake(location.coordinate, span);
    
    [_mkMapView setRegion:region animated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdatedLocation"
                                                        object:location];
    
}

- (void)locationError:(NSError *)error {
    NSLog(@"Error con la localización: %@", [error localizedDescription]);
}

#pragma mark - MapKit
- (void)dropPins {
    STKAnnotation *pin1 = [STKAnnotation.alloc initWithTitle:@"Softtek"
                                                    subtitle:@"Curso iOS"
                                              andCoordinates:CLLocationCoordinate2DMake(43.332342, -8.321231)];

    
    STKAnnotation *pin2 = [STKAnnotation.alloc initWithTitle:@"Softwhisper"
                                                    subtitle:@"Curso iOS 2"
                                              andCoordinates:CLLocationCoordinate2DMake(43.345342, -8.326231)];
    
    [_mkMapView addAnnotations:@[pin1, pin2]];
    
    [self drawPolyline];
}

- (void)drawPolyline {
    CLLocationCoordinate2D locations[[followHim count]];
    
    for (int i = 0; i < followHim.count; i++) {
        locations[i] = [((CLLocation *)[followHim objectAtIndex:i]) coordinate];
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:locations count:[followHim count]];
    [_mkMapView addOverlay:polyline];

}



- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    for (MKAnnotationView *v in views) {
        //v.image cambia el pin
        //v.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"phoenix.jpg"]];
    }
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay {
    
    MKPolylineView *polylineView = [MKPolylineView.alloc initWithOverlay:overlay];
    polylineView.strokeColor = [UIColor redColor];
    polylineView.lineWidth = 3.0f;
    
    return polylineView;
}


@end




















