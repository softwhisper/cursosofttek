//
//  AudioViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 10/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "AudioStreamer.h"
#import "AudioViewController.h"

@interface AudioViewController ()
@property (nonatomic, strong) AudioStreamer *player;
@end

@implementation AudioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *stream = @"http://95.81.147.10/4600/sky_151834.mp3";
    NSURL *url = [NSURL URLWithString:stream];
    _player = [[AudioStreamer alloc] initWithURL:url];
    
    [self becomeFirstResponder];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)play:(id)sender {
    [_player start];
}

- (IBAction)pause:(id)sender {
    [_player pause];
}

#pragma mark - Remote Events
- (void)remoteControlReceivedWithEvent:(UIEvent *)receivedEvent {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    switch (receivedEvent.subtype) {
        case UIEventSubtypeRemoteControlPlay:
            if (_player && ([_player isPaused])) {
                [_player start];
            }
            break;
            
        case UIEventSubtypeRemoteControlPause:
            if (_player && ([_player isPlaying])) {
                [_player pause];
            }
            break;
            
        case UIEventSubtypeRemoteControlStop:
            if (_player && ([_player isPlaying])) {
                [_player pause];
            }
            break;
        
        default:
            break;
    }
}

@end
