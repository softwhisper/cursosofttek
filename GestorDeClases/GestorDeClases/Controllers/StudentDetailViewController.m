//
//  StudentDetailViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 12/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "Student.h"
#import "SQLiteAccess+Student.h"
#import "StudentJsonClient.h"
#import "StudentDetailViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <GPUImage/GPUImage.h>
#import <UIImage+BlurAndDarken/UIImage+BlurAndDarken.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreGraphics/CoreGraphics.h>

@interface StudentDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (nonatomic, strong) Student *student;
@property (nonatomic, strong) UIImage *downloadedImage;

@end

@implementation StudentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /* BBDD SQLite
    _student = [SQLiteAccess selectByEmail:_email];
    [self setupUI];
    */
    // llamada al web service
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    StudentJsonClient *ws = StudentJsonClient.new;
    [ws getStudentForController:self andStudentId:_studentId];
    
    [_avatarImageView setCenter:CGPointMake(-160, 0)];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showNote:)
                                                 name:@"UpdatedLocation"
                                               object:nil];
}

- (void)animateImage {
    [UIView animateWithDuration:2.0f animations:^{
      // Animación
      [_avatarImageView setFrame:CGRectMake(0, 0, 640, 560)];
    
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            [_avatarImageView setFrame:CGRectMake(0, 0, 320, 280)];
        }];
    }];
    
    /*
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationDuration:1.0f];
    
    [_avatarImageView setFrame:CGRectMake(0, 0, 0, 0)];
    
    [UIView commitAnimations];
     */
    
    [self animateWithCA];
}

- (void)animateWithCA {
    
    [_avatarImageView setClipsToBounds:YES];
    CALayer *layer = _avatarImageView.layer;
    
    CABasicAnimation *basicAnim = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
    [basicAnim setDuration:5.0f];
    
    basicAnim.fromValue = [NSNumber numberWithFloat:0.0f];
    basicAnim.toValue = [NSNumber numberWithFloat:160.0f];
    
    [layer addAnimation:basicAnim forKey:@"cornerRadius"];
    [layer setCornerRadius:160.0f];
    
    [self performSelector:@selector(mergeImages) withObject:nil afterDelay:9];
}

- (void)mergeImages {
    // Fusionar ests dos imágenes
    //_downloadedImage
    UIImage *phoenixImg = [UIImage imageNamed:@"phoenix.jpg"];
    _downloadedImage = _avatarImageView.image;
    
    UIGraphicsBeginImageContext(CGSizeMake(320, 280));
    
    [_downloadedImage drawInRect:CGRectMake(160, 140, 160, 140)];
    [phoenixImg drawInRect:CGRectMake(0, 0, 160, 140)];
    [phoenixImg drawInRect:CGRectMake(0, 140, 160, 140)];
    [_nameLabel drawTextInRect:CGRectMake(80, 80, 100, 22)];
    
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    [_avatarImageView setImage:result];
}

- (void)showNote:(NSNotification *)notification {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, notification);
#endif
}

- (void)viewWillDisappear:(BOOL)animated {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif

}

- (void)setupUI {
    [_nameLabel setText:_student.name];
    [_cityLabel setText:_student.city];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSURL *url = [NSURL URLWithString:@"http://www.osmais.com/wallpapers/201204/lampadas-wallpaper.jpg"];
    
    /* No hacer nunca una petición sincrona en la UI
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    [_avatarImageView setImage:image]; */
    
    [_avatarImageView setImageWithURLRequest:[NSURLRequest requestWithURL:url]
                            placeholderImage:[UIImage imageNamed:@"phoenix.jpg"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                
                                [_avatarImageView setImage:image];
                                _downloadedImage = [UIImage imageWithCGImage:[image CGImage]];
                                [self animateImage];
                                
                            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                //
                            }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Gestures
// Longpress con 2 dedos durante un segundo
- (IBAction)blurOverImage:(id)sender {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    UIImage *tmp = [UIImage imageWithCGImage:[_avatarImageView.image CGImage]];
    [_avatarImageView setImage:[self blurWithGPUImage:tmp]];
}

// Single tap con 2 dedos
- (IBAction)unBlurImage:(id)sender {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    if (_downloadedImage) {
        [_avatarImageView setImage:_downloadedImage];
    }
}

- (UIImage *)blurWithGPUImage:(UIImage *)sourceImage {
    // Gaussian Blur
    GPUImageGaussianBlurFilter *blurFilter = [[GPUImageGaussianBlurFilter alloc] init];
    blurFilter.blurRadiusInPixels = 30.0;
    
    return [blurFilter imageByFilteringImage: sourceImage];
}

#pragma mark - Protocolo Network
- (void)receiveData:(NSMutableArray *)anArray {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif

    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)receiveStudent:(Student *)aStudent {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, [aStudent description]);
#endif
    
    _student = aStudent;
    [self setupUI];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)dataFailure:(NSString *)anErrorMessage {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:anErrorMessage
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
