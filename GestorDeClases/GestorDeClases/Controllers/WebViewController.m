//
//  WebViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 2/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, weak) IBOutlet UIWebView *webView;
@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:_urlString];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:req];
    
    [self.tabBarController.tabBar setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.tabBarController.tabBar setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Public Methods
- (void)setUrlString:(NSString *)anUrl {
    _urlString = [NSString stringWithString:anUrl];
}

#pragma mark - WebView Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) Start Req", __PRETTY_FUNCTION__, __LINE__);
#endif
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) End Request", __PRETTY_FUNCTION__, __LINE__);
#endif
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
}

- (IBAction)backOnWebView:(id)sender {
    if ([_webView canGoBack])
        [_webView goBack];
}

- (IBAction)forwardWebView:(id)sender {
    if ([_webView canGoForward]) {
        [_webView goForward];
    }
}

@end
