//
//  ClazzTableViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClazzTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SWNetworkDelegate>

@end
