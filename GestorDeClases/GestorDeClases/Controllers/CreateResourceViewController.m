//
//  CreateResourceViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 26/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "Resource.h"
#import "PostResourceJsonClient.h"
#import "CreateResourceViewController.h"

@interface CreateResourceViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (weak, nonatomic) IBOutlet UITextField *linkTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation CreateResourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions
- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)save:(id)sender {
    Resource *res = Resource.new;
    res.name = _nameTextField.text;
    res.rDescription = _descriptionTextField.text;
    res.link = _linkTextField.text;
    res.photo = _imageView.image;
    
    PostResourceJsonClient *ws = [[PostResourceJsonClient alloc] init];
    [ws postResource:res forController:self];
}

- (IBAction)selectImage:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancelar"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Cámara", @"Librería", nil];
    [actionSheet showInView:self.view];
}

#pragma mark
- (void)actionSheet:(UIActionSheet *)actionSheet
clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    [pickerController setAllowsEditing:YES];
    NSString *media = kUTTypeImage;
    pickerController.mediaTypes = @[media];

    if ((buttonIndex == 0) &&
        [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        // buttonIndex = 0 es Cámara
        [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        
    } else if (buttonIndex == 1) {
        
        // buttonIndex = 1 es Librería
        [pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
    } else {
        // Cancelar
        return;
    }
    
    [pickerController setDelegate:self];
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if ([info valueForKey:UIImagePickerControllerMediaType] == (NSString *)kUTTypeImage) {
        UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
        [_imageView setImage:image];
    } else {
        [_imageView setImage:[UIImage imageNamed:@"phoenix.jpg"]];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    // Por si el usuario cancela la acción
}

#pragma mark - SWNetworkDelegate
- (void)receiveData:(NSMutableArray *)anArray {
    [self cancel:nil];
}

- (void)dataFailure:(NSString *)anErrorMessage {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
