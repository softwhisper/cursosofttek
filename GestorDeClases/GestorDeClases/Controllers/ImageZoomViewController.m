//
//  ImageZoomViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import "ImageZoomViewController.h"

@interface ImageZoomViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImage *image;
@end

@implementation ImageZoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _imageView.image = _image;
    
    // Escalas y zoom
    [_scrollView setMinimumZoomScale:0.5f];
    [_scrollView setMaximumZoomScale:20.0f];
    [_scrollView setZoomScale:1.0f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setZoomImage:(UIImage *)aImage {
    _image = aImage;
}

#pragma mark - UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageView;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
#ifndef NDEBUG
    NSLog(@"END: %@ - %@", NSStringFromCGRect(scrollView.frame), NSStringFromCGSize(scrollView.contentSize));
#endif
    
    if (scale > 18.0f) {
        [scrollView setZoomScale:1.0f animated:YES];
    }
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
#ifndef NDEBUG
    NSLog(@"BEGIN: %@ - %@", NSStringFromCGRect(scrollView.frame), NSStringFromCGSize(scrollView.contentSize));
#endif
}

@end
