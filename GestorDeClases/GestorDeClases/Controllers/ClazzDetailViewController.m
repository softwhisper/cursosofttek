//
//  ClazzDetailViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "Clazz.h"
#import "ClazzDetailViewController.h"

@interface ClazzDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end

@implementation ClazzDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_nameLabel setText:[_clazz getName]];
    [_detailLabel setText:[_clazz getContent]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
