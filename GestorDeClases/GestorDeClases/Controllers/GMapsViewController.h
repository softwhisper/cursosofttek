//
//  GMapsViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 2/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "SWLocationManager.h"
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface GMapsViewController : UIViewController <GMSMapViewDelegate, MKMapViewDelegate, MKOverlay, SWLocationManagerDelegate>

@end
