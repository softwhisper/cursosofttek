//
//  ImageZoomViewController.h
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageZoomViewController : UIViewController <UIScrollViewDelegate>

- (void)setZoomImage:(UIImage *)aImage;

@end
