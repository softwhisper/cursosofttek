//
//  ResourcesCollectionViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 25/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import <AFNetworking/UIWebView+AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "ImageCollectionViewCell.h"
#import "WebCollectionViewCell.h"
#import "ResourcesCollectionViewController.h"
#import "ResourcesJsonClient.h"
#import "Resource.h"

@interface ResourcesCollectionViewController ()

@property (nonatomic, strong) NSMutableArray *resources;

@end

@implementation ResourcesCollectionViewController

static NSString *const imageCellReuseIdentifier = @"ImageCell";
static NSString *const webCellReuseIdentifier = @"WebCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _resources = NSMutableArray.new;
    
    ResourcesJsonClient *ws = [[ResourcesJsonClient alloc] init];
    [ws getResourcesForController:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showNote:)
                                                 name:@"UpdatedLocation"
                                               object:nil];
}

- (void)showNote:(NSNotification *)notification {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, notification);
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - SWNetworkDelegate
- (void)receiveData:(NSMutableArray *)anArray {
    _resources = [NSMutableArray arrayWithArray:anArray];
    [self.collectionView reloadData];
}

- (void)dataFailure:(NSString *)anErrorMessage {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:anErrorMessage
                                                   delegate:nil cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"imageDetailSegue"]) {
        NSIndexPath *pos = [self.collectionView indexPathsForSelectedItems][0];
        Resource *res = [_resources objectAtIndex:pos.row];
        [segue.destinationViewController performSelector:@selector(setZoomImage:) withObject:res.photo];
        
    } else if ([segue.identifier isEqualToString:@"webSegue"]) {
        NSIndexPath *pos = [self.collectionView indexPathsForSelectedItems][0];
        Resource *res = [_resources objectAtIndex:pos.row];
        [segue.destinationViewController performSelector:@selector(setUrlString:) withObject:res.link];
    }
}

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_resources count]; //Numero de resources
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    Resource *resource = [_resources objectAtIndex:indexPath.row];
    
    if ([resource.image_url containsString:@"/photos/"] && ![resource.image_url containsString:@"missing.png"]) {
#ifndef NDEBUG
        NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
        
        ImageCollectionViewCell *cell = [collectionView
                                         dequeueReusableCellWithReuseIdentifier:imageCellReuseIdentifier
                                                                   forIndexPath:indexPath];
        
        NSString *url = [@"http://curso.softwhisper.es" stringByAppendingString:resource.image_url];
        NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        
        [cell.imageView setImageWithURLRequest:req placeholderImage:[UIImage imageNamed:@"phoenix.jpg"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            resource.photo = image;
            cell.imageView.image = image;
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            NSLog(@"Fatal");
        }];
        
        return cell;
    } else {
#ifndef NDEBUG
        NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
        WebCollectionViewCell *cell = [collectionView
                                         dequeueReusableCellWithReuseIdentifier:webCellReuseIdentifier
                                         forIndexPath:indexPath];
        
        NSURL *url = [NSURL URLWithString:resource.link];
        NSURLRequest *req = [NSURLRequest requestWithURL:url];
        
        [cell.webView loadRequest:req
                         MIMEType:@"text/html"
                 textEncodingName:@"UTF-8"
                         progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            
        } success:^NSData *(NSHTTPURLResponse *response, NSData *data) {
            return data;
        } failure:^(NSError *error) {
            
        }];
        
        return cell;
    }
}

#pragma mark <UICollectionViewDelegate>

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}


/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
