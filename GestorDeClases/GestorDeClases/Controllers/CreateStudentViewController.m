//
//  CreateStudentViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estada on 24/11/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "Student.h"
#import "CreateStudentViewController.h"
#import "CreateStudentClient.h"

@interface CreateStudentViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation CreateStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
     */
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_nameTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Keyboard management
- (void)keyboardWillShown:(NSNotification *)notification {
    
    _scrollView.contentSize = _scrollView.frame.size;
    
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %@ \n %@", __PRETTY_FUNCTION__, __LINE__, notification, NSStringFromCGSize(_scrollView.contentSize));
#endif
    
    CGRect keyboardStartFrame = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSInteger animationCurve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    float animationDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    if (_scrollView.contentSize.height > _scrollView.frame.size.height) {
        return;
    }
    
    [UIView beginAnimations:@"keyboard" context:nil];
    [UIView setAnimationCurve:animationCurve];
    [UIView setAnimationDuration:animationDuration];
    
    CGRect finalScrollFrame = CGRectMake(0, 0, // coordenadas de orgin
                                         _scrollView.frame.size.width, // anchura
                                         _scrollView.frame.size.height - keyboardFrame.size.height); // altura
    
    [_scrollView setFrame:finalScrollFrame];
    
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    _scrollView.contentSize = _scrollView.frame.size;
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %@ \n %@", __PRETTY_FUNCTION__, __LINE__, notification, NSStringFromCGSize(_scrollView.contentSize));
#endif
    
    CGRect keyboardStartFrame = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSInteger animationCurve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    float animationDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView beginAnimations:@"keyboard" context:nil];
    [UIView setAnimationCurve:animationCurve];
    [UIView setAnimationDuration:animationDuration];
    
    CGRect finalScrollFrame = CGRectMake(0, 0, // coordenadas de orgin
                                         _scrollView.frame.size.width, // anchura
                                         _scrollView.frame.size.height + keyboardFrame.size.height); // altura
    
    [_scrollView setFrame:finalScrollFrame];
    
    
    [UIView commitAnimations];
}


#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [textField setBackgroundColor:[UIColor clearColor]];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"No puedes dejar el campo vacio"
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [textField setBackgroundColor:[UIColor redColor]];
        
        return YES;
    }
    
    if ([textField isEqual:_nameTextField]) {
        [_lastnameTextField becomeFirstResponder];
    } else if ([textField isEqual:_lastnameTextField]) {
        [_emailTextField becomeFirstResponder];
    } else if ([textField isEqual:_emailTextField]) {
        [_cityTextField becomeFirstResponder];
    } else if ([textField isEqual:_cityTextField]) {
        [_cityTextField resignFirstResponder];
        [self saveStudent:textField];
    }
    
    return YES;
}


#pragma mark - IBActions
- (IBAction)saveStudent:(id)sender {
    Student *student = Student.new;
    student.name = _nameTextField.text;
    student.lastname = _lastnameTextField.text;
    student.email = _emailTextField.text;
    student.city = _cityTextField.text;
    
    CreateStudentClient *ws = CreateStudentClient.new;
    [ws createStudent:student forController:self];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

#pragma mark - SWNetworkDelegate
- (void)receiveData:(NSMutableArray *)anArray {
    // Fue todo bien
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dataFailure:(NSString *)anErrorMessage {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:anErrorMessage
                                                   delegate:nil cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end















