//
//  ClazzTableViewController.m
//  GestorDeClases
//
//  Created by Pablo Formoso Estrada on 1/12/14.
//  Copyright (c) 2014 Pablo Formoso Estada. All rights reserved.
//
#import "Clazz.h"
#import "ClazzTableViewCell.h"
#import "ClazzTableViewController.h"
#import "ClazzDetailViewController.h"
#import "GetClazzesJsonClient.h"

@interface ClazzTableViewController ()

@property (nonatomic, strong) NSMutableArray *clazzes;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ClazzTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GetClazzesJsonClient *ws = [[GetClazzesJsonClient alloc] init];
    [ws getClazzesForController:self];
    _clazzes = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    Clazz *tmp = [_clazzes objectAtIndex:[_tableView indexPathForSelectedRow].row];
    
    if ([segue.destinationViewController isKindOfClass:ClazzDetailViewController.class]) {
        [((ClazzDetailViewController *)segue.destinationViewController) setClazz:tmp];
    }
}

#pragma mark - TableViewDataSource & Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    return [_clazzes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ClazzTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClazzCell" forIndexPath:indexPath];

    Clazz *cls = [_clazzes objectAtIndex:indexPath.row];
    
    [cell.nameLabel setText:[cls getName]];
    [cell.startAtLabel setText:[cls getStartDate]];
    [cell.endsAtLabel setText:[cls getEndsDate]];
    
    return cell;
}


#pragma mark - SWNetworkDelegate
- (void)receiveData:(NSMutableArray *)anArray {
    _clazzes = nil; _clazzes = [NSMutableArray arrayWithArray:anArray];
    [_tableView reloadData];
}

- (void)dataFailure:(NSString *)anErrorMessage {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:anErrorMessage
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
